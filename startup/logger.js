const appDebugger = require('../debuggers').app;
require('express-async-errors');

const winston = require ('winston');
const logger = winston.createLogger({
  level: 'error',
  format: winston.format.json(),
  transports: [
    new winston.transports.File({ filename: 'error.log', level: 'error' })
  ]
});

module.exports = function () {
  process.on('uncaughtException', (ex)=>{
    logger.error(ex.message); 
    appDebugger(ex.message);
    process.exit(1);
  });
  process.on('unhandledRejection', (ex)=>{
    logger.error(ex.message);
    appDebugger(ex.message);
    process.exit(1);
  });
}