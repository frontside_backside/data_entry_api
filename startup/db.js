const dbDebugger = require('../debuggers').db;
const config = require('config');
const mongoose = require('mongoose');

module.exports = function () {
  mongoose.connect(config.get('database'), {
      useNewUrlParser: true
    })
    .then(() => {
      dbDebugger('MongoDB connected...');
    });
};