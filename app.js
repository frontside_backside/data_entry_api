require('dotenv').config();
const appDebugger = require('./debuggers').app;
const config = require('config');
const express = require('express');
const app = express();

require('./startup/logger')();
require('./routes/routes')(app);
require('./startup/db')();

const port = process.env.PORT || config.get('port') || 3000;
app.listen(port, ()=>{ appDebugger(`Listening on ${port}...`) });