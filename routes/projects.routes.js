const httpDebugger = require('../debuggers').http;
const config = require('config');
const express = require('express');
const router = express.Router();
const asyncMiddleware = require('../middlewares/ async');

const {
  getProjects,
  getProject,
  createProject,
  updateProject,
  deleteProject,
  validateProject,
  validateProjectId,
  saveDocument,
  countDocuments
} = require('../models/project.model');


router.get('/', async (req, res) => {
    const result = await getProjects();
    res.send(JSON.stringify(result));
});

router.get('/:id', async (req, res) => {
  const { error } = validateProjectId(req.params.id);
  if (error) return res.status(400).send(error.details[0].message);

  const result= await getProject(req.params.id);
  if (result == null) return res.status(404).send('Not found');
  res.send(JSON.stringify(result));
});

router.post('/', async (req, res) => {
  const { error } = validateProject(req.body);
  if (error) return res.status(400).send(error.details[0].message);
  const project = {
    name: req.body.name,
    isActive: req.body.isActive,
    precission: req.body.precission,
    questions: req.body.questions
  };
 
  const result = await createProject(project);
  res.send(result);
});

router.put('/:id', async (req, res) =>{
  const { error } = validateProject(req.body);
  if (error) httpDebugger(error);
  if (error) return res.status(400).send(error.details[0].message);

  const result = await updateProject(req.params.id, req.body);
  res.send(result);    
});

router.delete('/:id', async (req, res) => {
  const { error } = validateProjectId(req.params.id);
  if (error) return res.status(400).send(error.details[0].message);

  const result = await deleteProject(req.params.id);
  res.send(result);
});

router.get('/:id/count', async (req, res) =>{
  const { error } = validateProjectId(req.params.id);
  if (error) return res.status(400).send(error.details[0].message);

  const count = await countDocuments(req.params.id);
  const result = {count: count};
  res.send(result);    
});

router.post('/:id/documents', async (req, res) =>{
  // const { error } = validateProject(req.body);
  // if (error) httpDebugger(error);
  // if (error) return res.status(400).send(error.details[0].message);

  const result = await saveDocument(req.params.id, req.body);
  res.send(result);    
});

module.exports = router;