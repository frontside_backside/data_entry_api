const httpDebugger = require('../debuggers').http;
const config = require('config');
const express = require('express');
const router = express.Router();
const asyncMiddleware = require('../middlewares/ async');
const {
  User,
  getUsers,
  getUser,
  createUser,
  validateUser
} = require('../models/user.model');

router.get('/', async (req, res) => {
  const role = req.user.role;
  if (role !== "admin" && role !== "manager") return res.status(403).send('Access denied.');  

  const result = await getUsers();
  res.send(result);  
});

router.get('/:id', async (req, res) => {
  const role = req.user.role;
  if (role !== "admin" && role !== "manager") return res.status(403).send('Access denied.');  

  const result = await getUser(req.params.id);
  res.send(result);  
});

router.post('/', async (req, res) => {
  const role = req.user.role;
  if (role !== "admin" && role !== "manager") return res.status(403).send('Access denied.');
 
  const {error} = validateUser(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  let user = await User.findOne( { login: req.body.login } );
  if (user) return res.status(400).send('User already exist.');

  const result = await createUser(req.body);
  res.send(result); 
});

router.put('/:id', async (req, res) => {
  const role = req.user.role;
  if (role !== "admin" && role !== "manager") return res.status(403).send('Access denied.');
 
  // const {error} = validateUser(req.body);
  // if (error) return res.status(400).send(error.details[0].message);

    const dublicate = await User.find({login: req.body.login, _id: {$ne: req.params.id} }).count();
    if (dublicate) return res.status(400).send('Invalid request.');

    const result = await User.findByIdAndUpdate(req.params.id, req.body, {new: true});
    res.send(result);  
});

router.delete('/:id', async (req, res)=>{
  const role = req.user.role;
  if (role !== "admin" && role !== "manager") return res.status(403).send('Access denied.');
  
  const result = await User.findByIdAndDelete(req.params.id);
  res.send(result);
});

module.exports = router;
