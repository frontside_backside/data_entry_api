const httpDebugger = require('../debuggers').http;
const express = require('express');
const cors = require('cors');
const helmet = require('helmet');
const morgan = require('morgan');
const error = require('../middlewares/error');

const login = require('./auth.routes');
const projects = require ('./projects.routes');
const users = require ('./users.routes');
const auth = require('../middlewares/auth');

module.exports = function (app) {
  app.use(cors());
  app.use(express.json());
  app.use(helmet()); // Secure http headers
  
  if(app.get('env') === 'development'){
    app.use(morgan('tiny'));
    httpDebugger('Morgan enabled...');
  }

  app.use('/api/auth', login); 
  app.use('/api/projects', auth, projects);
  app.use('/api/users', auth, users);

  app.use(error);
}