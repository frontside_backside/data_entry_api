const jwt = require('jsonwebtoken');
const config = require('config');

const {
  getUser
} = require('../models/user.model');

module.exports = async function(req, res, next){
  const token = req.header('x-auth-token');

  if (!token)
  return res.status(401).send('Access denied. No token provided.');

  try {
    const decoded = jwt.verify(token, config.get('jwtPrivateKey'));
  
    const user = await getUser(decoded._id);
    if (user === null) return res.status(401).send('Access denied.');
    if (decoded.role !== user.role) {decoded.role = user.role;}
    req.user = decoded;
    next();
  } catch (error) {
    res.status(400).send('Invalid token.');
  }

}