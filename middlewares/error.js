const dbDebugger = require('../debuggers').db;
const winston = require ('winston');
const logger = winston.createLogger({
  level: 'error',
  format: winston.format.json(),
  transports: [
    //
    // - Write to all logs with level `info` and below to `combined.log` 
    // - Write all logs error (and below) to `error.log`.
    //
    new winston.transports.File({ filename: 'error.log', level: 'error' })
  ]
});

module.exports = function(err, req, res, next){
  logger.error(err.message);
  dbDebugger(err.message);
  res.status(500).send('Internal Error.');
}