const debug = require('debug');

const http = debug('app:http');
const db = debug('app:db');
const app = debug('app:app');

module.exports = {
  http, db, app
}
