const mongoose = require('mongoose');
const {answerSchema} = require('./answer.model');
const Joi = require('joi');

const questionSchema = new mongoose.Schema({
  name: {
    type: String,
    minlength: 3,
    maxlength: 255,
    required: true
  },
  displayText: {
    type: String,
    maxlength: 255,
    default: this.name || ''
  },
  isActive:{
    type: Boolean,
    default: false
  },
  type: {
    type: Number,
    get: v => Math.round(v),
    set: v => Math.round(v),
    min: 0,
    max: 5
  },
  answers: {
    type: [answerSchema]
  }
});

const Question = new mongoose.model('Question', questionSchema);

function validateQuestion(question) {
  const schema = {
    name: Joi.string().min(3).max(255).required(),
    isActive: Joi.bool(),
    type: Joi.number().min(0).max(5),
    answers: Joi.array()
  }
  Joi.validate(question, schema);
}

module.exports.Question = Question;
module.exports.questionSchema = questionSchema;
module.exports.validateQuestion = validateQuestion;
