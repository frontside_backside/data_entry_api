const config = require('config');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const _ = require('lodash');
const mongoose = require('mongoose');
const Joi = require('joi');

const userSchema = new mongoose.Schema({
  name: {
    type: String,
    minlength: 5,
    maxlength: 255,
    required: true
  },
  login: {
    type: String,
    minlength: 5,
    maxlength: 50,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 1024
  },
  role: {
    type: String,
    enum: ['admin','manager','operator'],
    required: true
  },
  task:{
    type: String,
    default: ''
  }
});

userSchema.methods.generateAuthToken = function(){
  const token = jwt.sign({_id: this._id,role: this.role }, config.get('jwtPrivateKey'));
  return token;
};

const User = mongoose.model('User', userSchema);

async function getUsers() {
  const users = await User.find()
    .sort({
      name: 1
    })
    .select('-password');
  return users;
}

async function getUser(id) {
  const user = await User.findById(id)
    .select('-password');
  return user;
}

async function createUser(data){
  user = new User(_.pick(data, ['name', 'login', 'role', 'password']));

  const salt = await bcrypt.genSalt(10);
  user.password = await bcrypt.hash(user.password, salt);

  await user.save();

  return _.pick(user, ['_id', 'name', 'login']);
}

function validateUser(user) {
  const schema = {
    name: Joi.string().min(5).max(255).required(),
    login: Joi.string().min(5).max(50).required(),
    password: Joi.string().min(5).max(255).required(),
    role: Joi.string().regex(/(^admin$)|(^manager$)|(^operator$)/).required()
  }
  return Joi.validate(user, schema);
}

module.exports.User = User;
module.exports.getUsers = getUsers;
module.exports.getUser = getUser;
module.exports.createUser = createUser;
module.exports.validateUser = validateUser;