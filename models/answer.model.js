const mongoose = require('mongoose');

const answerSchema = new mongoose.Schema({
  value: {
    type: String,
    required: true
  },
  displayText: {
    type: String,
    // minlength: 3,
    maxlength: 255,
    default: this.value || ''
  },
  isActive: {
    type: Boolean,
    default: true
  }
});

const Answer = new mongoose.model('Answer', answerSchema);

module.exports.Answer = Answer;
module.exports.answerSchema = answerSchema;