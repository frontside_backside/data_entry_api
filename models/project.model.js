const dbDebugger = require('../debuggers').db;
const mongoose = require('mongoose');
const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);
const { questionSchema } = require('./question.model');

const projectSchema = new mongoose.Schema({
  name: {
    type: String,
    minlength: 3,
    maxlength: 50,
    required: true,
    unique: true
  },
  date: {
    type: Date,
    default: Date.now
  },
  isActive: {
    type: Boolean,
    default: false
  },
  precission: {
    type: Boolean,
    default: false
  },
  questions: {
    type: [questionSchema],
    default: []
  },
  count: Number
});

const Project = mongoose.model('Project', projectSchema);

async function getProjects() {
  const projects = await Project.find()
    .sort({
      name: 1,
      isActive: 1,
    });
  return projects;
}

async function getProject(id) {
  const project = await Project.findById(id);
  const count = await countDocuments(id);
  project.count = count;
  return project;
}

async function createProject(data) {
  const project = new Project();
  project.set({
    name: data.name,
    precission: data.precission || false,
    questions: data.questions || []
  });

  const result = await project.save();
  return result;  
}

async function updateProject(id, data) {
  const project = {
    name: data.name,
    isActive: data.isActive || false,
    precission: data.precission || false,
    questions: data.questions || []
  }

  const result = await Project.findByIdAndUpdate(id, project, {new: true});
  dbDebugger(result);
  return result;
}

async function saveDocument(id, data) {

  const result = await mongoose.connection.db.collection(id).insert({
    value: data
  });
  return result;
}

async function countDocuments(id) {
  const result = await mongoose.connection.db.collection(id).count();
  return result;
}

async function deleteProject(id) {
  const result = await Project.findByIdAndDelete(id);
  dbDebugger(result);
  return result;
}

function validateProject(project){
  const schema = {
    name: Joi.string().min(3).max(50).required(),
    date: Joi.date(),
    isActive: Joi.bool(),
    precission: Joi.bool(),
    questions: Joi.array(),
    _id: Joi.allow(),
    __v: Joi.allow()
  }

  return Joi.validate(project, schema);
}

function validateProjectId(id){
  const schema = {
    _id: Joi.objectId()
  }

  return Joi.validate({_id: id}, schema);
}

module.exports.Project = Project;
module.exports.getProjects = getProjects;
module.exports.getProject = getProject;
module.exports.createProject = createProject;
module.exports.updateProject = updateProject;
module.exports.deleteProject = deleteProject;
module.exports.validateProject = validateProject;
module.exports.validateProjectId = validateProjectId;
module.exports.saveDocument = saveDocument;
module.exports.countDocuments = countDocuments;